const timers = require('node:timers/promises');
const fs = require('node:fs/promises');

const MINUTE_IN_MS = 60 * 1000;

let isRecovering = false;

let errorCount = 0;

(async () => {
    for await (const _ of timers.setInterval(MINUTE_IN_MS)) {
        errorCount = 0;
        isRecovering = false;
    }
})()

/**
 * Mock function that
 */
function sendEmail() {
    console.log(`mail sent! at ${new Date().toISOString()}`)
}


/**
 * 
 * @param {Error} error 
 */
function logError(error) {
    errorCount++;

    if (errorCount >= 10 && !isRecovering) {
        sendEmail();

        isRecovering = true;
    }

    /**
     * Mocked file write logic
     * Please, ignore it :D
     */
    const currentDate = new Date();

    const fileSuffix = `${currentDate.getUTCFullYear()}${currentDate.getUTCMonth()}${currentDate.getUTCDay()}`

    const errorMessage = `${new Date().toISOString()} - ${error.message}\n`;

    fs.appendFile(`./error-${fileSuffix}.txt`, errorMessage)
        .catch(() => `ups!, error writing errors!`)
}


/**
 * Mocked errors happening in the server
 * Please, ignore it :D
 */
const ERRORS_MOCK_INTERVAL = 2 * 1000;

(async () => {
    for await (const _ of timers.setInterval(ERRORS_MOCK_INTERVAL)) {
        logError(new Error(`Strange error - id: ${Math.floor(Math.random() * 9999)}`))
    }
})()
