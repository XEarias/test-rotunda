class Animal {    
    /**
     * @type {string}
     */
    #sound;

    /**
     * 
     * @param {string} sound 
     */
    constructor(sound) {
        this.#sound = sound;
    }

    /**
     * 
     * @param {string} phrase 
     * @returns {string[]}
     */
    #getWords(phrase) {
        return phrase.split(' ').filter((word) => word.trim().length);
    }

    /**
     * 
     * @param {string[]} words 
     * @returns {string}
     */
    #getPhraseWithSounds(words) {
        return words.map((word) => `${word} ${this.#sound}`).join(' '); 
    }

    /**
     * 
     * @param {string} phrase 
     * @returns {string}
     */
    speak(phrase) {
        const words = this.#getWords(phrase);
        
        const phraseWithSounds = this.#getPhraseWithSounds(words);

        return phraseWithSounds;
    }
}

class Tiger extends Animal {
    /**
     * 
     * @param {Animal} prey 
     */
    hunt(prey) {
        console.log(`The tiger is hunting the ${prey.constructor.name}`);
    }
}

class Lion extends Animal {
    /**
     * 
     * @param {Animal} enemy 
     */
    fight(enemy) {
        console.log(`The lion is fighting the ${enemy.constructor.name}`);
    }
}

const lion = new Lion('roar');
console.log(lion.speak(`I'm a lion`));

const tiger = new Tiger('grrr');
console.log(tiger.speak('Lions suck'));

lion.fight(tiger);
tiger.hunt(lion);
