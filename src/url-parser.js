/**
 * @typedef ObjectRecord
 * @type {Object.<string, string | number | boolean>}
 */


class UrlParser {
    /**
     * @type {Map<string, number>}
     * @readonly
     */
    urlParamPositions = new Map();

    /**
     * 
     * @param {string} format 
     */
    constructor(format) {
        const urlFragments = format.split('/');

        for (const [index, urlFragment] of urlFragments.entries()) {
            if (!urlFragment.startsWith(':')) {
                continue;
            }

            const urlParam = urlFragment.replace(':', '');

            if (this.urlParamPositions.has(urlParam)) {
                throw new Error('Error! you are duplicating keys');
            }

            this.urlParamPositions.set(urlParam, index)
        }
    }

    /**
     * 
     * @param {string} param 
     * @returns {string | number}
     */
    #parseParam(param) {
        const parsedParam = parseFloat(param);

        if (isNaN(parsedParam)) {
            return param;
        }

        return parsedParam;
    }

    /**
     * 
     * @param {string} path 
     * @returns {ObjectRecord}
     */
    #getParams(path) {
        const urlFragments = path.split('/');
        const params = {};

        for (const [paramName, paramIndex] of this.urlParamPositions.entries()) {
            params[paramName] = this.#parseParam(urlFragments[paramIndex]);
        }

        return params;
    }

    /**
     * 
     * @param {string} query 
     * @returns {ObjectRecord}
     */
    #getQueryArgs(query) {
        const queryFragments = query.split('&');

        const queryArgs = {}

        for (const queryFragment of queryFragments) {
            const [queryName, queryValue] = queryFragment.split('=');

            queryArgs[queryName] = this.#parseParam(queryValue);
        }

        return queryArgs;
    }

    /**
     * 
     * @param {string} url 
     * @returns {ObjectRecord}
     */
    parseURL(url) {
        const [path, query] = url.split('?');

        const params = this.#getParams(path);

        const queryArgs = query ? this.#getQueryArgs(query) : {};

        return { ...params, ...queryArgs };
    }
}


const urlParser = new UrlParser('/:version/api/:collection/:id');

console.log(urlParser.parseURL('/6/api/listings/3?sort=desc&limit=10'));
console.log(urlParser.parseURL('/6/api/listings/3'));